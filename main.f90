module gl_f90_mod

    use opengl_gl
    use opengl_glu
    use opengl_glut
    implicit none
    
    integer(GLint) :: wW = 1024, wH = 1024
    integer(glcint) window

    integer, allocatable :: cells(:,:,:)

    integer :: i,j,ni,nj,h

    real :: tic, toc
    
    contains

    subroutine getNeighbourSum(a,b,s)
        integer, intent(in) :: a,b
        integer, intent(out) :: s
        s = 0

        ni = a-1
        nj = b-1
        s = s + cells(ni,nj,1)
        
        ni = a
        s = s + cells(ni,nj,1)

        ni = a+1
        s = s + cells(ni,nj,1)

        nj = b
        s = s + cells(ni,nj,1)

        nj = b+1
        s = s + cells(ni,nj,1)

        ni = a
        s = s + cells(ni,nj,1)

        ni = a-1
        s = s + cells(ni,nj,1)

        nj = b
        s = s + cells(ni,nj,1)

        !print *, s
    end subroutine

    subroutine updateCells()
        integer :: s
        call glBegin(GL_POINTS)
        do i = 1,wW
            do j = 1,wH
                call getNeighbourSum(i,j,s)
                if (cells(i,j,1) == 1 .and. s < 2) then
                    cells(i,j,2) = 0
                else if (cells(i,j,1) == 1 .and. (s >= 2 .and. s <= 3)) then
                    cells(i,j,2) = 1
                else if (cells(i,j,1)==1 .and. s > 3) then
                    cells(i,j,2) = 0
                else if (cells(i,j,1)==0 .and. s == 3) then
                    cells(i,j,2) = 1
                end if
                if (cells(i,j,2) == 1) then
                    call glColor3f(1.0,1.0,1.0)
                else
                    call glColor3f(0.0,0.0,0.0)
                end if
                call glVertex2f(i-1.0,j-1.0)
            end do
        end do
        cells(:,:,1) = cells(:,:,2)
        cells(:,:,2) = 0
        call glEnd()
    end subroutine
    
    subroutine display() bind(c)
        call glViewport(0_GLint,0_GLint,wW,wH)
        call glMatrixMode(GL_PROJECTION)
        call glLoadIdentity()
        call gluOrtho2D(0.0_gldouble,wW+0.0_gldouble,0.0_gldouble,wH+0.0_gldouble)
        call glClearColor(1.0,1.0,1.0,1.0)
        call glClear(GL_COLOR_BUFFER_BIT)
        call glColor3f(1.0,0.0,0.0)
        
        !> enter some fancy draw code here!
        !> call drawSomeCoolStuff(...)
        call updateCells()

        call glFlush()

        call cpu_time(tic)
        toc = tic

        do while (toc-tic < 1.0/60.0)
            call cpu_time(toc)
        end do

        call glutPostRedisplay()
    end subroutine display
    
    subroutine key(ikey, x, y) bind(c)
        INTEGER(GLbyte), VALUE :: ikey
        INTEGER(GLint), VALUE :: x, y
        print *, ikey
        select case(ikey)
        case (27)
          stop
        end select
    end subroutine
    
    subroutine init()
        integer(GLenum) type
    
        call glutInitWindowSize(wW,wH)
        call glutInit()
        type = GLUT_RGB
        type = ior(type,GLUT_SINGLE)
        call glutInitDisplayMode(type)
        window = glutCreateWindow("GL and Fortran!")
        call glutDisplayFunc(display)
        call glutKeyboardFunc(key)
           
        call glutMainLoop()
    
    end subroutine init
    
    end module gl_f90_mod
    
    program main
        use opengl_gl
        use opengl_glut
        use gl_f90_mod

        allocate(cells(wW,wH,2))

        do i = 1,wW
            do j = 1,wH
                cells(i,j,1) = rand()>0.5
            end do
        end do
    
        call init()
    end program main
    